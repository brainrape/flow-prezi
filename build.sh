#!/bin/sh

HEADER=$(cat <<EOF
<!DOCTYPE html>
<html>
  <head>
    <title>Workflow</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Sans:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Sans'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      h3#start-from-master, h3#short-lived-feature-branch, h3#merge-request, h3#merge{
        margin-top: 30px;
        margin-bottom: 0px;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
    </style>
  </head>
  <body>
    <textarea id="source">
EOF
)

FOOTER=$(cat <<EOF
    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
EOF
)


echo ""
echo "$HEADER"
cat prezi.md
echo "$FOOTER"
