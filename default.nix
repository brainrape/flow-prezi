{pkgs ? import <nixpkgs> {}, ...}:
pkgs.stdenv.mkDerivation {
  name = "prezi";
  buildInputs = [ pkgs.inotify-tools ];
}
