# the·flow - high level goals

### value oriented development

  - ship fast
  - be safe
  - focus on value, not chores

### principled but pragmatic

  - logical foundations - single source of truth
  - define all the things - exact (hash based) dependencies
  - confine unknowns - be honest about what we don't know and can't control
  - work well with existing tech

---

### the essence of CI

## master always green - fresh, valid, deployed

&nbsp;

### the·flow

## master ⇒ issue ⇒ branch ⇒ mr ⇒ merge + deploy

&nbsp;

### GitLab Flow

a great starting point

What: https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/

Why: https://docs.gitlab.com/ce/workflow/gitlab_flow.html

---

# the·flow

### start from master

- [always start with an issue](https://about.gitlab.com/2016/03/03/start-with-an-issue/)
- if it is worth spending time on, it is worth describing

### small feature branches

- one branch per issue
- short-lived - a few hours to a day
- test all pushes

### merge request

- start a discussion to validate, review, and improve the solution

### merge

- only possible when tests pass and discussions are resolved
- CI automatically deploys new master

---

# infrastructure

&nbsp;

## it is part of our software, so treat it as such

## enter the·flow

&nbsp;

### extras

- update plan for all branches
- one click deploy/destroy of any branch
- production branch deployed in separate environment
